let colorObj = {
    "rgb(27,32,37)": "color",
    "rgb(0,0,0)": "color",
    "rgb(34,39,46)": "color",
    "rgb(55,65,75)": "color",
    "rgb(77,89,105)": "color",
    "rgb(98,115,134)": "color",
    "rgb(124,141,159)": "color",
    "rgb(154,165,181)": "color",
    "rgb(184,191,202)": "color",
    "rgb(213,218,224)": "color",
    "rgb(242,244,246)": "color",
    "rgb(248,249,250)": "color",
    "rgb(255,255,255)": "color",
    "rgb(24,86,173)": "color",
    "rgb(27,109,198)": "color",
    "rgb(31,136,222)": "color",
    "rgb(34,165,247)": "color",
    "rgb(89,188,249)": "color",
    "rgb(145,210,251)": "color",
    "rgb(189,228,253)": "color",
    "rgb(211,237,253)": "color",
    "rgb(233,246,254)": "color",
    "rgb(244,251,255)": "color",
    "rgb(171,60,82)": "color",
    "rgb(196,69,86)": "color",
    "rgb(220,77,87)": "color",
    "rgb(245,86,86)": "color",
    "rgb(248,128,128)": "color",
    "rgb(250,171,171)": "color",
    "rgb(252,204,204)": "color",
    "rgb(253,221,221)": "color",
    "rgb(254,238,238)": "color",
    "rgb(255,247,247)": "color",
    "rgb(179,97,8)": "color",
    "rgb(204,124,9)": "color",
    "rgb(230,155,10)": "color",
    "rgb(255,188,11)": "color",
    "rgb(255,205,72)": "color",
    "rgb(255,222,133)": "color",
    "rgb(255,235,182)": "color",
    "rgb(255,242,206)": "color",
    "rgb(255,248,231)": "color",
    "rgb(255,252,243)": "color",
    "rgb(38,146,94)": "color",
    "rgb(43,166,100)": "color",
    "rgb(49,187,103)": "color",
    "rgb(54,208,104)": "color",
    "rgb(104,220,142)": "color",
    "rgb(155,232,180)": "color",
    "rgb(195,241,210)": "color",
    "rgb(215,246,225)": "color",
    "rgb(235,250,240)": "color",
    "rgb(245,253,247)": "color",
    "rgb(22,56,150)": "color",
    "rgb(26,73,171)": "color",
    "rgb(29,93,193)": "color",
    "rgb(32,116,214)": "color",
    "rgb(88,151,224)": "color",
    "rgb(144,186,235)": "color",
    "rgb(188,213,243)": "color",
    "rgb(210,227,247)": "color",
    "rgb(233,241,251)": "color",
    "rgb(244,248,253)": "color",
    "rgb(144,47,77)": "color",
    "rgb(164,54,80)": "color",
    "rgb(185,60,82)": "color",
    "rgb(205,67,82)": "color",
    "rgb(218,114,125)": "color",
    "rgb(230,161,169)": "color",
    "rgb(240,199,203)": "color",
    "rgb(245,217,220)": "color",
    "rgb(250,236,238)": "color",
    "rgb(253,246,246)": "color",
    "rgb(125,44,0)": "color",
    "rgb(143,59,0)": "color",
    "rgb(161,77,0)": "color",
    "rgb(179,98,0)": "color",
    "rgb(198,137,64)": "color",
    "rgb(217,177,128)": "color",
    "rgb(232,208,179)": "color",
    "rgb(240,224,204)": "color",
    "rgb(247,239,230)": "color",
    "rgb(251,247,242)": "color",
    "rgb(15,94,75)": "color",
    "rgb(18,107,80)": "color",
    "rgb(20,121,83)": "color",
    "rgb(22,134,85)": "color",
    "rgb(80,164,128)": "color",
    "rgb(139,195,170)": "color",
    "rgb(185,219,204)": "color",
    "rgb(208,231,221)": "color",
    "rgb(232,243,238)": "color",
    "rgb(243,249,247)": "color"
}

let fontSize = {
    '24px': 'text',
    '18px': 'text',
    '14px': 'text',
    '12px': 'text',
    '0px': 'text'
};

let fontWeight = {
    '400': 'weight',
    '600': 'weight',
    '500': 'weight',
};

let fontFamily = {
    'NutanixSoft': 'fontFamily',
    'Monaco-Regular': 'fontFamily',
    'nutanix-icon': 'fontFamily'
}

function RGBToHex(rgb) {
    // Choose correct separator
    let sep = rgb.indexOf(",") > -1 ? "," : " ";
    // Turn "rgb(r,g,b)" into [r,g,b]
    rgb = rgb.substr(4).split(")")[0].split(sep);

    let r = (+rgb[0]).toString(16),
        g = (+rgb[1]).toString(16),
        b = (+rgb[2]).toString(16);

    if (r.length == 1)
        r = "0" + r;
    if (g.length == 1)
        g = "0" + g;
    if (b.length == 1)
        b = "0" + b;

    return "#" + r + g + b;
}

let issuesObj = {};
let mainObj = {}

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  console.log(document);
  console.log(document.querySelectorAll("div"));
  console.log(request);
    issuesObj = {};
    mainObj = {
        issues: {},
        screenshot: ""
    };
    var elems = document.body.getElementsByTagName("*");
    for (let i = 0; i < elems.length; i++) {
      let node = elems[i];
      let computedNode = window.getComputedStyle(node)
        for (var key in computedNode) {
            if (computedNode.hasOwnProperty(key)) {
                let obj = checker(node, computedNode, key, request);
                if (obj) {
                    if (!mainObj.issues[obj.type]) {
                        mainObj.issues[obj.type] = [];
                    }
                    mainObj.issues[obj.type].push(obj)
                }
            }
        }
    }
    let pageText = [];
    function getInnerMostText(node) {
        if (node.nodeType == 3) {
            // Filter out text nodes that contain only whitespace
            if (!/^\s*$/.test(node.data)) {
                pageText.push(node.data)
            }
        } else if (node.hasChildNodes()) {
            for (var i = 0, len = node.childNodes.length; i < len; ++i) {
                getInnerMostText(node.childNodes[i]);
            }
        }
    }
    getInnerMostText(document.body);

    html2canvas(document.body).then(function(canvas) {
        mainObj.screenshot = canvas.toDataURL();
        let fullUrl = window.location.href;
        let lastUrl = fullUrl.split('/');
        let resObj = {
            issuesObj: issuesObj,
            mainObj: mainObj,
            'fullUrl': fullUrl,
            'lastUrl': lastUrl[lastUrl.length - 1],
            'issueTime': new Date(),
            pageText
        };
        console.log(resObj)
        axios.post('http://localhost:4000/issues', resObj)
        sendResponse(resObj)
    });

});

function checker (node, computedNode, key, request) {
    let returnObj = {}
    if (request['color']) {
        // Color checker
        returnObj = colorChecker(node, computedNode, key);
    }
    switch (key) {
        case 'fontSize':
            if (request['fontSize']) {
                returnObj = fontSizeCheck(node, computedNode, key);
            }
            break;
        case 'fontWeight':
            if (request['fontWeight']) {
                returnObj = fontWeightCheck(node, computedNode, key);
            }
            break;
        case 'fontFamily':
            if (request['fontFamily']) {
                returnObj = fontFamilyCheck(node, computedNode, key);
            }
            break;
    }
    return returnObj

}

function fontSizeCheck(node, computedNode, key) {
    let cleanKey = computedNode[key].replace(/\s/g,'')
    if(!fontSize.hasOwnProperty(cleanKey)) {
        if (issuesObj && issuesObj['fontSizeIssue']) {
            issuesObj['fontSizeIssue']++;
        } else {
            issuesObj['fontSizeIssue'] = 1;
        }
        console.log("---------------------------------------")
        console.log(node);
        node.style.border = "1px solid red";
        console.log(key + '---> ' + computedNode[key])
        console.log("Font size: " + computedNode[key])
        console.log("---------------XXXXXXXXX----------------");
        return issueObjCreator(key, computedNode, node, 'fontSize');
    }
}

function fontWeightCheck(node, computedNode, key) {
    let cleanKey = computedNode[key].replace(/\s/g,'')
    if(!fontWeight.hasOwnProperty(cleanKey)) {
        if (issuesObj && issuesObj['fontWeightIssue']) {
            issuesObj['fontWeightIssue']++;
        } else {
            issuesObj['fontWeightIssue'] = 1;
        }
        console.log("---------------------------------------")
        console.log(node);
        node.style.border = "1px solid red";
        console.log(key + '---> ' + computedNode[key])
        console.log("Font Weight: " + computedNode[key])
        console.log("---------------XXXXXXXXX----------------");
        return issueObjCreator(key, computedNode, node, 'fontWeigth');
    }
}

function fontFamilyCheck(node, computedNode, key) {
    let cleanKey = computedNode[key].replace(/\s/g,'')
    let obj = {};
    if(!fontFamily.hasOwnProperty(cleanKey)) {
        if (issuesObj && issuesObj['fontFamilyIssue']) {
            issuesObj['fontFamilyIssue']++;
        } else {
            issuesObj['fontFamilyIssue'] = 1;
        }
        console.log("---------------------------------------")
        console.log(node)
        node.style.border = "1px solid red";
        console.log(key + '---> ' + computedNode[key])
        console.log("Font Family: " + computedNode[key])
        console.log("class Names:" + node.className);
        console.log("---------------XXXXXXXXX----------------")
        return issueObjCreator(key, computedNode, node, 'fontFamily');
    }
}

function colorChecker(node, computedNode, key) {
    if ((key.includes('color') || key.includes('Color')) && computedNode[key].includes('rgb(')) {
        let cleanKey = computedNode[key].replace(/\s/g,'')
        if(!colorObj.hasOwnProperty(cleanKey) && computedNode[key] !== "rgb(255,0,0)" && computedNode[key] !== "rgb(255, 0, 0)" && computedNode[key] !== "rgb(255, 0, 0)") {
          if (issuesObj && issuesObj['colorIssue']) {
              issuesObj['colorIssue']++;
          } else {
              issuesObj['colorIssue'] = 1;
          }
            console.log("---------------------------------------")
            console.log(node)
            node.style.border = "1px solid red";
            console.log(key + '---> ' + computedNode[key])
            console.log("In Hex: " + RGBToHex(computedNode[key]))
            console.log("---------------XXXXXXXXX----------------");
            console.log(issueObjCreator(key, computedNode, node, 'color'));
            return issueObjCreator(key, computedNode, node, 'color');
        }
    }
}

function issueObjCreator(key, computedNode, node, issueType) {
    return {
        'type': key,
        'issueValue': computedNode[key],
        'issueType': issueType,
        'className': node.className
    }
}